import React, { Component } from 'react';
import './App.css';
import {connect} from "react-redux"
import ListItem from "./components/listitem"
import Post from "./components/post/index.jsx"
import {CSSTransition, TransitionGroup} from 'react-transition-group'
import {v4} from 'uuid'

function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

async function loadUsers() {
  await sleep(4000);
  return {
    users: [
      1,2,3,
    ]
  }
}

class App extends Component {

  constructor(props) {
    super(props);
    this.addPost = this.addPost.bind(this);
    this.state = {
      postList:[{ 
        id: 1,
        text: 'this is your microblog!', 
        likes: 0
      }, { 
        id: 2,
        text: 'enjoy your blogging experience', 
        likes: 0
      }]
    };

    this.removeElem = this.removeElem.bind(this);
  }


  removeElem(removeIndex) {
    this.setState({postList: 
        this.state.postList.filter((item,index) => 
        index !== removeIndex)})
  }

  getUsers = () => {
    this.setState({loading: true});
    loadUsers().then(({users}) => {
      console.log(users);
      this.setState({loading: false})
    })
  }

  componentDidMount() {
     this.getUsers();
  }
  
  addPost(e) {
    if (this._inputElement.value !== "") {
      var newItem = {
        text: this._inputElement.value.slice(0,140),
        key: Date.now(),
        id:v4()
      };
   
      this.setState((prevState) => {
        return { 
          postList: prevState.postList.concat(newItem) 
        };
      });
     
      this._inputElement.value = "";
    }
     
    console.log(this.state.postList);
       
    e.preventDefault();
  }

  render() {
    return <>

    <CSSTransition in={this.state.loading}
    timeout={300}
    unmountOnExit
    classNames={{
      enter:'enter',
      enterActive:'enter-active',
      exit: 'leave',
      exitActive: 'leave-active'
    }}>
      <div>
      {this.state.loading ? 'loading':'loaded'}
      </div>
    </CSSTransition>

    <div className="postListMain">
      <div className="header">
      <form onSubmit={this.addPost}>
        <input ref={(a) => this._inputElement = a}
            placeholder="your post here, max 140 symbols">
        </input>
        <button type="submit">post</button>
      </form>
      </div>
      <div class='last-liked'>
      Last liked post:{this.props.test.data}
      </div>
    </div>
    
    <TransitionGroup className="posts">
    
    {this.state.postList.map((item, index) =>
      <CSSTransition key={item.id}
                     timeout={300}
                     unmountOnExit
                     classNames={{
                       enter:'enter',
                       enterActive:'enter-active',
                       exit: 'leave',
                       exitActive: 'leave-active'
                     }}>
        <Post  title={item.text}
                onRemove={() => this.removeElem(index)}/>
      </CSSTransition>)} 
  </TransitionGroup>

    </>
  }

}

export default connect (
  state => ({test: state.test})
)(App);
