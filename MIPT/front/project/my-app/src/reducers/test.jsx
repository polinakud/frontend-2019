import {CHANGE_TEST_DATA} from '../actions';

const initialState = {
    postList:[{ 
          id: 5,
          text: 'Hi, welcome to your microblog', 
          likes: 0
        }],
  
    };

const testReducer = (state = initialState, action) => {
    switch (action.type) {
        case CHANGE_TEST_DATA:
            return {...state, ...action.payload};
        default:
            return state;
    }
};

export default testReducer;