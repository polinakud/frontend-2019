import {createStore, combineReducers, applyMiddleware} from 'redux';
import testReducer from './test';

import * as types from '../constants/index.js';
import omit from 'lodash/omit';
import assign from 'lodash/assign';
import mapValues from 'lodash/mapValues';


const logger = store => next => action => {
    let result;
    console.groupCollapsed('dispatching', action.type);
    console.log('[rev state', store.getState());
    console.log('action', action);
    result = next(action);
    console.log('next state', store.getState());
    console.groupEnd();
    return result;
}

const initialState = {
  postList:[{ 
        id: 5,
        text: 'Hi, welcome to your microblog', 
        likes: 0
      }],

  };



export default function posts(state = initialState, action) {
  switch (action.type) {

    case types.ADD_POST:
      const newId = state.posts[state.posts.length-1] + 1;
      return {
        posts: state.posts.concat(newId),
        postById: {
          ...state.postById,
          [newId]: {
            id: newId,
            text: action.text,
            likes: 0
          }
        }
      }

      case types.DELETE_POST:
      return {
        ...state,
        posts: state.posts.filter(id => id !== action.id),
        postById: omit(state.postById, action.id)
      }

    case types.LIKE_POST:
      return {
        ...state,
        postById: mapValues(state.postById, (post) => {
          return post.id === action.id ?
            assign({}, post, { likes: post.likes+1 }) :
            post
        })
      }

    default:
      return state;
  }
}

const saver = store => next => action => {
    let result = next(action);
    localStorage['mipt-web'] = JSON.stringify(store.getState());
    return result;
}

const initStorage = (initialState = {}) => {
    return localStorage['mipt-web'] ? JSON.parse(localStorage['mipt-web']) : initialState;
}

export const storeFactory = (initialState = {}) => (
    applyMiddleware(logger, saver)(createStore) (
        combineReducers({
            test: testReducer,
        }), initStorage(initialState)
    )
)