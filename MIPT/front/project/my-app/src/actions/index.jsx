export const ADD_POST = 'ADD_POST';
export const LIKE_POST = 'LIKE_POST';
export const DELETE_POST = 'DELETE_POST';

export function addPost(text) {
  return {
    type: ADD_POST,
    text
  };
}

export function deletePost(id) {
  return {
    type: DELETE_POST,
    id
  };
}

export function likePost(id) {
  return {
    type: LIKE_POST,
    id
  };
}

export const CHANGE_TEST_DATA = 'CHANGE_TEST_DATA';

export const changeTest = title => ({
    type: CHANGE_TEST_DATA,
    payload: {data:title}
});