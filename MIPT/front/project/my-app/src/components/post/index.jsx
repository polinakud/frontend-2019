import React from 'react'
import './styles.css'
import {connect} from "react-redux"
import {changeTest} from '../../actions'

class Post extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            likes: 0,
        };
    }

    addLikes = () => {
        this.setState({likes: this.state.likes+1});
        this.props.changeComponentTest(this.props.title);
    }

    render() {
        return <div className='post'>
                <div class='text'> 
                    {this.props.title}  
                    
                </div>
                <div class='buttons'>
                <div class='likes' onClick={this.addLikes}>
                <button type="submit"> 💕 {this.state.likes}</button>
                </div>
                <div class='remove' onClick={this.props.onRemove}>
                    <button type="submit">  delete </button>
                </div>
                
                </div>
                <div class={this.state.likes>0 ? 
                'liked':'unliked'} >
                    <button type="submit">  🌠 </button>
                </div>
        </div>;
    }
}

export default connect (
    state => ({test: state.test}),
    dispatch => ({
        changeComponentTest(text) {
            dispatch(changeTest(text));
        }
    })
) (Post)