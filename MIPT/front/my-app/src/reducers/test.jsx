import {CHANGE_TEST_DATA} from '../actions';

const initialState = {
    data: "Hello world",
}

const testReducer = (state = initialState, action) => {
    switch (action.type) {
        case CHANGE_TEST_DATA:
            return {...state, ...action.payload};
        default:
            return state;
    }
};

export default testReducer;