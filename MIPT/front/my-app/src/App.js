import React, { Component } from 'react';
import './App.css';
import {connect} from "react-redux"
import ListItem from "./components/listitem"
import {CSSTransition, TransitionGroup} from 'react-transition-group'
import {v4} from 'uuid'

function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

async function loadUsers() {
  await sleep(4000);
  return {
    users: [
      1,2,3,
    ]
  }
}

class App extends Component {

  constructor(props) {
    super(props);

    this.state = {
      data : [ 
        { 
          id: 1,
          title: 'Work 1', 
          priority: 1 
        }, 
        { 
          id: 2,
          title: 'Study 2', 
          priority: 10 
        }, 
        { 
          id: 3,
          title: 'Sleep 3', 
          priority: -10 
        },
        { 
          id: 4,
          title: 'Repeat 4', 
          priority: 50
        } 
      ] 
    };

    this.removeElem = this.removeElem.bind(this);
  }


  removeElem(removeIndex) {
    this.setState({data: 
        this.state.data.filter((item,index) => 
        index !== removeIndex)})
  }

  addItem = () => {
    this.setState({
      data: [
        ...this.state.data,
        {title: 'Sleep', priority:1, id:v4()}
      ]
    })
  }

  getUsers = () => {
    this.setState({loading: true});
    loadUsers().then(({users}) => {
      console.log(users);
      this.setState({loading: false})
    })
  }
  componentDidMount() {
     this.getUsers();
  }
  

  render() {
    return <>

    <CSSTransition in={this.state.loading}
    timeout={300}
    unmountOnExit
    classNames={{
      enter:'enter',
      enterActive:'enter-active',
      exit: 'leave',
      exitActive: 'leave-active'
    }}>
      <div>
      {this.state.loading ? 'loading':'loaded'}
      </div>
    </CSSTransition>
    
    
    <TransitionGroup className="App">
        {this.props.test.data}
        {this.state.data.map((item, index) =>
          <CSSTransition key={item.id}
                         timeout={300}
                         unmountOnExit
                         classNames={{
                           enter:'enter',
                           enterActive:'enter-active',
                           exit: 'leave',
                           exitActive: 'leave-active'
                         }}>
            <ListItem  title={item.title}
                    onRemove={() => this.removeElem(index)}
                    priority={item.priority}/>
          </CSSTransition>)} 
        <div onClick={this.addItem}>
            Add item
        </div>
      </TransitionGroup>
    </>
  }
}

export default connect (
  state => ({test: state.test})
)(App);
