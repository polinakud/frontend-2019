import React from 'react'
import './styles.css'
import {connect} from "react-redux"
import {changeTest} from '../../actions'

class ListItem extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            checked: false,
        };

        //this.changeChecked = this.changeChecked.bind(this); Для избавления = () => - показываем ф-ии changeChecked какой у нее this
    }

    changeChecked = () => {
        this.setState({checked: !this.state.checked});
        this.props.changeComponentTest(this.props.title);
    }

    render() {
        return <div className={this.state.checked ? 
                'list-item-checked':'list-item'}>
                <div onClick={this.changeChecked}> 
                    {`${this.props.title} ${this.props.priority}`}
                </div>
                <div onClick={this.props.onRemove}>
                    remove
                </div>
        </div>;
    }
}

export default connect (
    state => ({test: state.test}),
    dispatch => ({
        changeComponentTest(text) {
            dispatch(changeTest(text));
        }
    })
) (ListItem)