import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';

//ReactDOM.render(<App />, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA

import  {BrowserRouter} from 'react-router-dom';
import MainRouter from './routes';
import {Provider} from 'react-redux';
import { storeFactory } from './reducers';
const initialState = {
    //test:'Hello',
    //data: {
    //    title:'title'
    //}
}

export const store = storeFactory(initialState);


const render = () =>
    ReactDOM.render(
        <Provider store={store}>
            <BrowserRouter>
                <MainRouter/>
            </BrowserRouter>,
        </Provider>,
        document.getElementById('root'));

store.subscribe(render);
render();

serviceWorker.unregister();
