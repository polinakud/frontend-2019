export const CHANGE_TEST_DATA = 'CHANGE_TEST_DATA';

export const changeTest = title => ({
    type: CHANGE_TEST_DATA,
    payload: {data:title}
});